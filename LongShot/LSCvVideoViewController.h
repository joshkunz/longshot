//
//  LSCvVideoViewController.h
//  LongShot
//
//  Created by Josh Kunz on 4/20/13.
//  Copyright (c) 2013 Josh Kunz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <opencv2/highgui/cap_ios.h>

#import "LSCameraToolbarView.h"
#import "LSCameraToolbarDelegate.h"

@interface LSCvVideoViewController : UIViewController<CvVideoCameraDelegate, LSCameraToolbarDelegate> {
    UIView *_main_view;
    UIImageView * _image_view;
    CvVideoCamera * _camera;
    
    LSCameraToolbarView * _toolbar;
}

- (id)init;

@end
