//
//  LSCameraToolbarView.h
//  LongShot
//
//  Created by Josh Kunz on 4/20/13.
//  Copyright (c) 2013 Josh Kunz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LSCameraToolbarDelegate.h"
#import "LSCameraButtonDelegate.h"

@interface LSCameraToolbarView : UIView<LSCameraButtonDelegate>

@property (weak) id<LSCameraToolbarDelegate> delegate;

- (void) setRecording:(BOOL)state;
- (BOOL) recording;

@end
