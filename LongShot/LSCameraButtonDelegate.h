//
//  LSCameraButtonDelegate.h
//  LongShot
//
//  Created by Josh Kunz on 4/20/13.
//  Copyright (c) 2013 Josh Kunz. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol LSCameraButtonDelegate <NSObject>

/* this button has been tapped */
- (void) camera_button_tapped;

@end
