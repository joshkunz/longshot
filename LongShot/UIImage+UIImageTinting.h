//
//  UIImage_UIImageTinting.h
//  LongShot
//
//  Created by Josh Kunz on 4/20/13.
//  Copyright (c) 2013 Josh Kunz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (UIImageTinting)

- (UIImage *)tintUsingColor:(UIColor *)tintColor;

@end