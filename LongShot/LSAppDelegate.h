//
//  LSAppDelegate.h
//  LongShot
//
//  Created by Josh Kunz on 4/16/13.
//  Copyright (c) 2013 Josh Kunz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LSAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
