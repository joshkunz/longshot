//
//  LSAppDelegate.m
//  LongShot
//
//  Created by Josh Kunz on 4/16/13.
//  Copyright (c) 2013 Josh Kunz. All rights reserved.
//

#import "LSAppDelegate.h"
#import "LSCvVideoViewController.h"

@implementation LSAppDelegate

- (void)dealloc
{
    [_window release];
    [super dealloc];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    /* hide the status bar */
    [application setStatusBarHidden:YES];
    self.window = [[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] autorelease];
    
    /* Create and set the root view controller */
    UINavigationController * root_controller = [[UINavigationController alloc] init];
    [self.window setRootViewController:root_controller];
    /* hide the navigation bar */
    root_controller.navigationBarHidden = true;
    
    /* Add our custom root view controller */
    LSCvVideoViewController * video_controller = [[LSCvVideoViewController alloc] init];
    [root_controller pushViewController:video_controller animated:NO];
    
    [self.window makeKeyAndVisible];
    
    [video_controller release];
    [root_controller release];
    return YES;
}

@end
