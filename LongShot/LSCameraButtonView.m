//
//  LSCameraButtonView.m
//  LongShot
//
//  Created by Josh Kunz on 4/20/13.
//  Copyright (c) 2013 Josh Kunz. All rights reserved.
//

#import "LSCameraButtonView.h"
#import "LSCameraButtonDelegate.h"
#import "UIImage+UIImageTinting.h"

#define TAP_REVERT_WAIT 0.2

@implementation LSCameraButtonView {
    CGGradientRef _background_gradient;
    CGGradientRef _tapped_gradient;
    CGGradientRef _notactive_gradient;
    UIImageView *_camera_icon;
    UIGestureRecognizer * _tap_recognizer;
    BOOL _tapped;
    BOOL _active;
    
    id<LSCameraButtonDelegate> _delegate;
}

@synthesize delegate = _delegate;

- (id)initWithFrame:(CGRect)frame andGradient:(CGGradientRef)grad
                            andTappedGradient:(CGGradientRef)tapped
                      andDisactivatedGradient:(CGGradientRef)deactived {
    self = [super initWithFrame:frame];
    if (self) {
        /* Store the supplied gradiant */
        _background_gradient = grad;
        CGGradientRetain(_background_gradient);
        
        _tapped_gradient = tapped;
        CGGradientRetain(_tapped_gradient);
        
        _notactive_gradient = deactived;
        CGGradientRetain(deactived);
        
        /* Set the background to be clear */
        [self setBackgroundColor:[UIColor clearColor]];
        
        /* Get the image, and tint it */
        UIImage * camera_icon_image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"86-camera"
                                                                                                       ofType:@"png"]];
        UIColor * tint_color = [UIColor colorWithWhite:0.9f alpha:0.7f];
        UIImage * tinted_image = [camera_icon_image tintUsingColor:tint_color];
        
        _camera_icon = [[UIImageView alloc] initWithImage:tinted_image];
        [self addSubview:_camera_icon];
        
        _tap_recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapped)];
        [self addGestureRecognizer:_tap_recognizer];
        
        _tapped = NO;
        _active = YES;
    }
    return self;
}

- (void)dealloc {
    /* Release our gradiant */
    CGGradientRelease(_background_gradient);
    CGGradientRelease(_tapped_gradient);
    
    [_tap_recognizer release];
    [_camera_icon release];
    [super dealloc];
}

- (void) setActive:(BOOL)activity {
    _active = activity;
    NSLog(@"Setting activity");
    [self setNeedsDisplay];
}

# pragma mark - GestureRecognizer Methods

- (void) tapped {
    if (! _active) { return; }
    _tapped = TRUE;
    [self setNeedsDisplay];
    [_delegate camera_button_tapped];
}

# pragma mark - Drawing Code

- (void) layoutSubviews {
    /* center the icon on the center of the button */
    _camera_icon.center = CGPointMake(self.bounds.size.width / 2,
                                      self.bounds.size.height / 2);
}

- (void) revert_tapped {
    _tapped = NO;
    [self setNeedsDisplay];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation. */
- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGFloat radius = self.bounds.size.height / 2;
    
    /* Upper flat */
    CGContextMoveToPoint(context, radius, self.bounds.origin.y);
    CGContextAddLineToPoint(context, self.bounds.size.width - radius, self.bounds.origin.y);
    
    /* right cap */
    CGContextAddArc(context,
                    self.bounds.size.width - radius,
                    self.bounds.size.height / 2,
                    radius, M_PI + (M_PI / 2), M_PI /2, 0);
    
    /* Bottom flat */
    CGContextAddLineToPoint(context, radius, self.bounds.size.height);
    
    /* left Cap */
    CGContextAddArc(context, radius,
                    self.bounds.size.height / 2,
                    radius, M_PI / 2, M_PI + (M_PI / 2), 0);
    
    /* Close the path */
    CGContextClosePath(context);
    
//    CGContextSetStrokeColorWithColor(context, [[UIColor blueColor] CGColor]);
//    CGContextStrokePath(context);
    
    /* Clip the gradiant to the path */
    CGContextClip(context);
    
    CGGradientRef draw_gradient = _background_gradient;
    
    /* draw the tapped gradiant if tapped */
    if (_tapped) {
        draw_gradient = _tapped_gradient;
        /* timer to revert the gradient */
        [NSTimer scheduledTimerWithTimeInterval:TAP_REVERT_WAIT
                                         target:self
                                       selector:@selector(revert_tapped)
                                       userInfo:nil
                                        repeats:NO];
    } else if (! _active) {
        draw_gradient = _notactive_gradient;
    }
    
    /* Draw in the background gradiant */
    CGContextDrawLinearGradient(context, draw_gradient,
                                CGPointMake(self.bounds.size.width / 2, self.bounds.origin.y),
                                CGPointMake(self.bounds.size.width / 2, self.bounds.size.height),
                                0);
}
// */

@end
