//
//  LSCameraToolbarView.m
//  LongShot
//
//  Created by Josh Kunz on 4/20/13.
//  Copyright (c) 2013 Josh Kunz. All rights reserved.
//

#import "LSCameraToolbarView.h"
#import "LSCameraButtonView.h"
#import "UIImage+UIImageTinting.h"

/* show the button for accessing the camera roll (doesn't work */
//#define PHOTO_BUTTON

/* show the 'recording' status indicator */
#define STATUS_INDICATOR
/* show the button for taking pictures */
#define CAMERA_BUTTON

@implementation LSCameraToolbarView {
    id<LSCameraToolbarDelegate> _delegate;
    CGGradientRef _bar_gradient;
    LSCameraButtonView * _snapshot_button;
    UIImageView * _photos;
    UIImageView * _recording;
    
    BOOL _recording_now;
}

@synthesize delegate = _delegate;

# pragma mark - Initializer

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        /* Create our gradiant */
        CGColorSpaceRef rgb_space = CGColorSpaceCreateDeviceRGB();
        CGFloat locations[2] = { 0.0f, 1.0f };
        
        /* gradient for the bar */
        CGFloat bar_gradient_components[8] = {
            0.6367f, 0.6367f, 0.6367f, 1.0f, /* start color */
            0.2148f, 0.2148f, 0.2148f, 1.0f /* end color */
        };
        
        /* gradiant for the un-tapped button */
        CGFloat button_gradient_components[8] = {
            0.2148f, 0.2148f, 0.1248f, 1.0f,    /* Starting color */
            0.0f, 0.0f, 0.0f, 1.0f              /* Ending Color */
        };
        
        /* Gradient for the tapped button */
        CGFloat tapped_button_gradient_components[8] = {
            0.2148f, 0.2148f, 0.1248f, 1.0f,    /* Starting color */
            0.2f, 0.2f, 0.2f, 1.0f              /* Ending Color */
        };
        
        /* create the gradient */
        _bar_gradient = CGGradientCreateWithColorComponents(rgb_space,
                                                            bar_gradient_components,
                                                            locations, 2);
        /* Gradient for the camera button */
        CGGradientRef button_gradient = CGGradientCreateWithColorComponents(rgb_space,
                                                                            button_gradient_components,
                                                                            locations, 2);
        
        CGGradientRef tapped_gradient = CGGradientCreateWithColorComponents(rgb_space,
                                                                            tapped_button_gradient_components,
                                                                            locations, 2);
#ifdef CAMERA_BUTTON
        /* Create our button */
        _snapshot_button = [[LSCameraButtonView alloc] initWithFrame:CGRectZero
                                                         andGradient:button_gradient
                                                   andTappedGradient:tapped_gradient
                                             andDisactivatedGradient:tapped_gradient];
        /* Make ourselves the button's delegate */
        _snapshot_button.delegate = self;
        
        [self addSubview:_snapshot_button];
#endif
     
#ifdef PHOTO_BUTTON
        UIColor * tint_color = [UIColor colorWithWhite:0.9f alpha:0.7f];
        /* Make the photo images */
        UIImage * photo_icon_image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"42-photos"
                                                                                                      ofType:@"png"]];
        UIImage * tinted_photo_icon = [photo_icon_image tintUsingColor:tint_color];
        _photos = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0,
                                                                tinted_photo_icon.size.width * 1.2f,
                                                                tinted_photo_icon.size.height * 1.2f)];
        [_photos setImage:tinted_photo_icon];
        
        [self addSubview:_photos];
#endif
        
        /* Add the recording Icon view */
        _recording_now = NO;
#ifdef STATUS_INDICATOR
        UIImage * recording_icon = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"160-voicemail-2"
                                                                                                    ofType:@"png"]];
        recording_icon = [recording_icon tintUsingColor:[UIColor colorWithRed:0.341176f
                                                                        green:1.0f
                                                                         blue:0.27843f
                                                                        alpha:1.0f]];
        _recording = [[UIImageView alloc] initWithImage:recording_icon];
        _recording.hidden = YES;
        [self addSubview:_recording];
#endif
        
        /* Release our unused variables */
        CGGradientRelease(button_gradient);
        CGGradientRelease(tapped_gradient);
        CGColorSpaceRelease(rgb_space);
    }
    
    return self;
}

# pragma mark Destructor

- (void) dealloc {
    /* release our gradiant */
    CGGradientRelease(_bar_gradient);
    [super dealloc];
}

# pragma mark - API methods

- (void) setRecording:(BOOL)state {
    _recording_now = state;
    _recording.hidden = !state;
    [_snapshot_button setActive:!state];
    
    /* Update the state */
    [_recording setNeedsDisplay];
}

- (BOOL) recording {
    return _recording_now;
}

# pragma mark - LSCameraButtonView Delegate methods

- (void) camera_button_tapped {
    [_delegate camera_button_tapped];
}

# pragma mark - Drawing

- (void)layoutSubviews {
    /* set the new frame for the button */
    CGRect button_frame = CGRectInset(self.bounds,
                                      self.bounds.size.width / 4,
                                      self.bounds.size.height * 0.1f);
    [_snapshot_button setFrame:button_frame];
    [_snapshot_button setNeedsDisplay];
    
    _photos.center = CGPointMake(self.bounds.size.width * 0.1f,
                                 self.bounds.size.height / 2);
    [_photos setNeedsDisplay];
    
    _recording.center = CGPointMake(self.bounds.size.width - (self.bounds.size.width * 0.1f),
                                    self.bounds.size.height / 2);
    
    [_recording setNeedsDisplay];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation. */
- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    /* Draw in the background gradient */
    CGContextDrawLinearGradient(context, _bar_gradient,
                                CGPointMake(self.bounds.size.width / 2, self.bounds.origin.y),
                                CGPointMake(self.bounds.size.width / 2, self.bounds.size.height),
                                0);
}
// */

@end
