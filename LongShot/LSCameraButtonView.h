//
//  LSCameraButtonView.h
//  LongShot
//
//  Created by Josh Kunz on 4/20/13.
//  Copyright (c) 2013 Josh Kunz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LSCameraButtonDelegate.h"

@interface LSCameraButtonView : UIView

- (id) initWithFrame:(CGRect)frame
         andGradient:(CGGradientRef)grad
   andTappedGradient:(CGGradientRef)tapped
andDisactivatedGradient:(CGGradientRef)deactived;

- (void) setActive:(BOOL)activity;

@property (weak) id<LSCameraButtonDelegate> delegate;

@end
