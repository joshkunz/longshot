//
//  LS.m
//  LongShot
//
//  Created by Josh Kunz on 4/20/13.
//  Copyright (c) 2013 Josh Kunz. All rights reserved.
//

#import "UIImage+UIImageTinting.h"

@implementation UIImage (UIImageTinting)

- (UIImage *)tintUsingColor:(UIColor *)tintColor {
    UIGraphicsBeginImageContextWithOptions(self.size, NO, 1.0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGRect rect = CGRectMake(0, 0, self.size.width, self.size.height);
    
    // draw original image
    [self drawInRect:rect blendMode:kCGBlendModeNormal alpha:1.0f];
    
    // tint image (loosing alpha).
    // kCGBlendModeOverlay is the closest I was able to match the
    // actual process used by apple in navigation bar
    CGContextSetBlendMode(context, kCGBlendModeLighten);
    [tintColor setFill];
    CGContextFillRect(context, rect);
    
    // mask by alpha values of original image
    [self drawInRect:rect blendMode:kCGBlendModeDestinationIn alpha:1.0f];
    
    UIImage *tintedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return tintedImage;
}

@end
