//
//  LSCvVideoViewController.m
//  LongShot
//
//  Created by Josh Kunz on 4/20/13.
//  Copyright (c) 2013 Josh Kunz. All rights reserved.
//

#import "LSCvVideoViewController.h"
#import <opencv2/highgui/cap_ios.h>
#import <opencv2/nonfree/gpu.hpp>
#import "LSCameraToolbarView.h"
#import "LSCameraToolbarDelegate.h"
#import "UIImage+CVMat.h"

#define FPS 30
#define CAPTURE_FRAMES (15)

@interface LSCvVideoViewController () {
    BOOL _has_shot;
    cv::Mat * _image;
    int _frames;
}

@end

@implementation LSCvVideoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        _has_shot = FALSE;
        _image = NULL;
    }
    return self;
}

- (id)init {
    return [self initWithNibName:nil bundle:nil];
}

# pragma mark - CvVideoCamera Deleage methods

- (void)averageFrames:(cv::Mat &)image {
    
    /* make the saved image easier to work with */
    cv::Mat &saved_image = *_image;
    
    // accept only char type matrices
    CV_Assert(image.depth() != sizeof(uchar));
    CV_Assert(saved_image.depth() != sizeof(uchar));

    /* Make sure the images map */
    if (image.channels() != saved_image.channels()) { NSLog(@"Saved image doesn't match"); return;}
    if (image.rows != saved_image.rows) { NSLog(@"Saved image doesn't match [rows]"); return; }
    if (image.cols != saved_image.cols) { NSLog(@"Saved image doesn't match [cols]"); return; }
    if (image.isContinuous() != saved_image.isContinuous()) {
        NSLog(@"Saved images doesn't match [continuous]");
    }
    
    /* Get the information about this image */
    int channels = image.channels();
    
    int num_rows = image.rows,
        num_cols = image.cols * channels;
    
    if (image.isContinuous()) {
        num_cols *= num_rows;
        num_rows = 1;
    }
    
    /* iterate through all of the pixel values */
    uchar* row_pointer, * _row_pointer;
    for(int i = 0; i < num_rows; ++i) {
        row_pointer = image.ptr<uchar>(i);
        _row_pointer = saved_image.ptr<uchar>(i);
        
        for (int j = 0; j < num_cols; ++j) {
            /* average the pixel values */
            _row_pointer[j] = (row_pointer[j] + _row_pointer[j]) / 2;
            /* Display the changes */
            row_pointer[j] = _row_pointer[j];
        }
    }
}

- (void)processImage:(cv::Mat &)image {
    if (! _has_shot) { return; }
    /* Check if we've captured all of the neccicary frames */
    if (self->_frames == CAPTURE_FRAMES) {
        /* re-set in selector */
        self->_has_shot = NO;

        /* Save the image to the camera roll */
        UIImageWriteToSavedPhotosAlbum([UIImage imageFromCVMat:*self->_image], nil, nil, nil);
        /* Turn remove the status indicator */
        [NSThread detachNewThreadSelector:@selector(stop_record) toTarget:self withObject:nil];

        /* kill the image buffer */
        delete self->_image;
        self->_image = NULL;
        
        return;
    }
    
    /* If we have no locally-stored image, make a copy of the frame */
    if (self->_image == NULL) {
        self->_image = new cv::Mat;
        *(self->_image) = image.clone();
        /* this is a new image, we have nothing to average */
        return;
    }
    
    /* Average the frames */
    NSLog(@"Processing the Image...");
    [self averageFrames:image];
    
    /* Increment the number of frames we've processed */
    self->_frames++;
}

# pragma mark - LSCameraToolbarView Delegate methods

- (void) stop_record {
    _toolbar.recording = NO;
}

/* Start recording when the button gets tapped */
- (void) camera_button_tapped {
    _has_shot = TRUE;
    _frames = 0;
    _toolbar.recording = YES;
}

# pragma mark - SubView Handling

- (void) loadView {
    CGRect main_view_frame = [[UIScreen mainScreen] bounds];
    /* Account for the status bar */
    _main_view = [[UIView alloc] initWithFrame:main_view_frame];
    self.view = _main_view;
    CGRect slice, remainder;
    /* Slice the view up into two different quadrants, 
     one for the camera, and one for the picture-taking button */
    CGRectDivide(self.view.frame, &slice, &remainder, self.view.bounds.size.height * 0.87, CGRectMinYEdge);
    _image_view = [[UIImageView alloc] initWithFrame:slice];
    _toolbar = [[LSCameraToolbarView alloc] initWithFrame:remainder];
    /* set ourself as the toolbar's delegate */
    _toolbar.delegate = self;
    [self.view addSubview:_image_view];
    [self.view addSubview:_toolbar];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _camera = [[CvVideoCamera alloc] initWithParentView:_image_view];
    _camera.defaultAVCaptureDevicePosition = AVCaptureDevicePositionBack;
    _camera.defaultAVCaptureVideoOrientation = AVCaptureVideoOrientationPortrait;
    _camera.defaultAVCaptureSessionPreset = AVCaptureSessionPreset352x288;
//    _camera.defaultAVCaptureSessionPreset = AVCaptureSessionPreset640x480;
    _camera.defaultFPS = FPS;
    /* Add ourselves as the delegate */
    [_camera setDelegate:self];
    /* Start the camera up */
    [_camera start];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
