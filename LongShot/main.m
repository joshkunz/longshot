//
//  main.m
//  LongShot
//
//  Created by Josh Kunz on 4/16/13.
//  Copyright (c) 2013 Josh Kunz. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "LSAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([LSAppDelegate class]));
    }
}
